# Course: CS 485 - Data Mining
# Assignment: Class Project
# Dataset: Kaggle - Shelter Animal Outcomes
# Team: Ryan Taber and Hunter O'Rourke

#------------------------------------------------------------------------------#
#   File Purpose: 
# 	
#	run svm classifier(s) on the shelter animal outcomes dataset
#
#------------------------------------------------------------------------------#

import sys
import numpy as np
import scipy as sp
import sklearn as skl
from sklearn import preprocessing, svm
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import random, math,time
import datetime

#------------------------------------------------------------------------------#


def runSVM(tr_samp, te_samp, tr_out, te_out):
	
	kTypes = ['linear','poly','rbf','sigmoid']

	
	for k in kTypes:

		clf = svm.SVC(kernel = k)
		libsvm_start = time.time()

		clf.fit(tr_samp, tr_out)
		libsvm_train_end = time.time()

		a = np.array(clf.predict(te_samp).astype(int))
		libsvm_test_end = time.time()
		c = a == np.array(te_out)
		c = np.array(a) == np.array(te_out)

		print "\nUsing kernel = " + k
		print "Accuracy                                    : ",str(sum(c) * 100.0/len(a)) + " %"
		print "Number of support vectors for positive class: " ,clf.n_support_[0]
		print "Number of support vectors for negative class: ",clf.n_support_[1]
		print "Time Required for training		   : ",str(libsvm_train_end - libsvm_start)+" s"
		print "Time Required for testing  		   : ",str(libsvm_test_end-libsvm_train_end)+" s"
  

def scaleData():

	filtered_data = np.array(np.genfromtxt('filtered.txt', delimiter = ','))

	scaled_filtered_data = preprocessing.scale(filtered_data)

#	print scaled_filtered_data

	outcome = scaled_filtered_data[:,2] #scaled_filtered_data[:,2]
	scaled_filtered_data = scaled_filtered_data[:,:-1]

	outcome2 = np.array([-1]*len(outcome))
	
	print outcome
	print outcome2
	for i in range(0,len(outcome2)):
		if outcome[i] > 0:
			outcome2[i] = 1
	print outcome2

	#-- PARTITION DATA --#
	training_size_fraction = .10
	test_idx = np.array(random.sample(xrange(len(outcome2)), int((1 - training_size_fraction) * len(outcome2))))
	train_idx = np.array([x for x in xrange(len(outcome2)) if x not in test_idx])
	test_samples = scaled_filtered_data[test_idx]
	train_samples = scaled_filtered_data[train_idx]
	test_out = outcome2[test_idx]
	train_out = outcome2[train_idx]

	#get rid of NaNs before call to SVM
	train_samples = np.nan_to_num(train_samples)
	test_samples = np.nan_to_num(test_samples)


	#call runSVM()
	runSVM(train_samples, test_samples, train_out, test_out)

def processData():

	#-- DATA HEADERS --#
	data_headers = ['AnimalID','Name','DateTime','OutcomeType','OutcomeSubtype','AnimalType','SexuponOutcome','AgeuponOutcome','Breed','Color'] 

	data= np.array(np.genfromtxt('t.txt', dtype=('S32','S32','S32','S32','S32','S32','S32','S50','S32')\
			,delimiter=',',autostrip=True, names=data_headers, usecols=("Name","DateTime","OutcomeType",'OutcomeSubtype','AnimalType',\
			'SexuponOutcome','AgeuponOutcome','Breed','Color') ))
	
	data.shape
	print data.shape
	

	#Replace empty values with 'None'
#	replaceCount = 0
#	for i in range(0, len(data)):
	#	for d in i: 
#		if ('' in data[i]):
#			for d in data[i]:
#				if d == '':
#					d = 'None'
#					replaceCount += 1
	
#	print replaceCount, "records have incomplete data"


	
	#now set up the class attribute lists
	type_attr = ['Dog','Cat']
	sex_attr = ['Intact Female','Intact Male','Neutered Male','Spayed Female','Unknown'] 
	outcome_attr = ['Euthanasia','Died','Adoption','Transfer','Return_to_owner']	
	
	with open("../Data/out/names.out") as name: 
		name_attr = name.read().splitlines() 
	with open("../Data/out/colors.out") as color:
		color_attr = color.read().splitlines()
	with open("../Data/out/breeds.out") as breed:
		breed_attr = breed.read().splitlines()
	with open("numDays.txt") as date:
		date_attr = date.read().splitlines()
	with open("../Data/out/ages.out") as age:
		age_attr = age.read().splitlines()
	with open("../Data/train/suboutcomes.train") as sub:
		sub_attr = sub.read().splitlines()


#	temp_date_attr = convertDate(temp_date_attr)
	
	#-- DATE --#
	idx_date = {}
	for d in date_attr:
		idx_date[d] = data["DateTime"] == d

	for i in range(0,len(date_attr)):
		data["DateTime"][idx_date[date_attr[i]]] = int(i)
	#	temp_date_attr[idx_date[date_attr[i]]] = int(i)

	#-- OUTCOME --#
	idx_out = {}
	for o in outcome_attr:
		idx_out[o] = data["OutcomeType"] == o

	for i in range(0, len(outcome_attr)):
		data["OutcomeType"][idx_out[outcome_attr[i]]] = int(i)


	#-- SUBOUTCOME --#
	idx_sub = {}
	for su in sub_attr:
		idx_sub[su] = data["OutcomeSubtype"] == su

	for i in range(0, len(sub_attr)):
		data["OutcomeSubtype"][idx_sub[sub_attr[i]]] = int(i)

	#-- ANIMAL TYPE --#
	idx_type = {}
	for t in type_attr:
		idx_type[t] = data["AnimalType"] == t

	for i in range(0, len(type_attr)):
		data["AnimalType"][idx_type[type_attr[i]]]=int(i)


	#-- SEX --#
	idx_sex = {}
	for s in sex_attr:
		idx_sex[s] = data["SexuponOutcome"] == s

	for i in range(0, len(sex_attr)):
		data["SexuponOutcome"][idx_sex[sex_attr[i]]]=int(i)


	#-- NAMES  --#
	idx_name = {}
	for n in name_attr:
		idx_name[n] = data["Name"] == n

	for i in range(0, len(name_attr)):
		data["Name"][idx_name[name_attr[i]]]=int(i)


	#-- COLORS  --#
	idx_color = {}
	for c in color_attr:
		idx_color[c] = data["Color"] == c

	for i in range(0, len(color_attr)):
		data["Color"][idx_color[color_attr[i]]]=int(i)


	#-- BREEDS  --#
	idx_breed = {}
	for b in breed_attr:
		idx_breed[b] = data["Breed"] == b

	for i in range(0, len(breed_attr)):
		data["Breed"][idx_breed[breed_attr[i]]]=int(i)


	#-- AGE --#
	idx_age = {}
	for a in age_attr:
		idx_age[a] = data["AgeuponOutcome"] == a

	for i in range(0, len(age_attr)):
		data["AgeuponOutcome"][idx_age[age_attr[i]]]=int(i)


#	a = plt.hist(data['DateTime'],12)
#	means = [ (a[1][i] + a[1][1+i])/2.0 for i in range(0,len(a[1])-1)]
#	means, a[0]

#	idx_date_0 = data['DateTime'] <= 105702
#	idx_date_1 = (data['DateTime'] >= 105702) & (data['fnlwgt'] <= 289569)
#	idx_date_2 = data['DateTime'] >= 289569



	

	#-- SAVE FILTERED DATA --#
	np.savetxt('filtered.txt', data, fmt = "%s", delimiter = ',')



#-----------------------------------------------------------------------------#
def main(argv): 

	processData()
	scaleData()

#-----------------------------------------------------------------------------#

if __name__ == '__main__':
	main(sys.argv)
