\documentclass[11pt,letterpaper,titlepage]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern} 
\usepackage{amsmath, amsfonts, amssymb, microtype, makeidx, enumitem, hyperref, titlesec, float, tabu}
\usepackage[margin=1.25in]{geometry}
\usepackage[final]{pdfpages}
\usepackage[section]{placeins}
\usepackage{graphicx}
\usepackage{pgfplots}
\usepackage{array}
\usepackage{pgfplotstable}
\graphicspath{ {images/} }
\usepackage{color, colortbl}
\renewcommand{\thesection}{\Roman{section}} 
\renewcommand{\thesubsection}{\thesection.\Roman{subsection}}
\titlespacing\subparagraph{12pt}{8pt plus 1pt minus 2pt}{2pt plus 2pt minus 2pt}
\DisableLigatures{encoding = *, family = *}

\begin{document}
\definecolor{Gray}{gray}{0.9}
\title{\LARGE{Project Proposal}\\\Large{CS 485: Data Mining\\University of Kentucky}}
\author{Hunter O'Rourke, Ryan Taber}
\date{April 5, 2016}
\pagenumbering{arabic}
\maketitle
\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Project Description}
This section lays out the general problem behind the Shelter Animal Outcomes (SAO) dataset. A brief description of the background and data involved are provided. Following these descriptions, a discussion of the goals for the project and a proposed approach to reach them ensues.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Background and Motivation}
The SAO competition appears on \url{https://kaggle.com} as one of the many open public data mining projects. This particular dataset contains information on roughly 38,000 dogs and cats that entered the Austin Animal Center over the course of two and half years. The Austin Animal Center is the largest no-kill animal shelter in the United States, and they process nearly 18,000 animals each year. A quick sort of the training data shows the animals for which data is provided were housed at the shelter for some span between October 2013 and February 2016.\\

\raggedright The ultimate goal of the project is to implement data mining techniques to predict the outcome of shelter animals given a set of attributes particular to each animal. The outcome classes are divided into these options - Adoption, Died, Euthanasia, Return to Owner, Transfer. \\

Refer to the following subsections to learn more about the dataset and the proposed approach for prediction. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
\subsection{Dataset Description}
The SAO dataset is split into two csv files - test and train - both of which have been randomly selected from the complete dataset. Because the SAO project is largely a classification problem, the data has been divided into training and test sets. In terms of the complete attribute set, the training and test sets contain the same fields except that the outcome information has been scrubeed from the test set. \textbf{Tables 1 and 2} give a brief description of the attributes in the dataset with example data and the sizes of each subset. 

\paragraph*{}
\begin{center}
\textbf{Table 1: Animals per Dataset\\}

\begin{tabular}{ |m{3cm}|| m{3cm}|}
\hline
\rowcolor{Gray}
\hline
 \textbf{Dataset} & \textbf{Size} \\
\hline\hline
Training & 26729  \\
\hline
Testing & 11456 \\
\hline
\end{tabular}
\end{center}

\clearpage

\begin{center}
\textbf{Table 2: Dataset Feature Descriptions\\}
\begin{tabular}{| m{2.8cm}|| m{4.5cm} || m{2.7cm} |}
\hline
\rowcolor{Gray}
 \textbf{Attribute} & \textbf{Description} & \textbf{Which Dataset} \\
\hline\hline
AnimalID & Unique identifier of animal, alphanumeric & Both\\
\hline
Name &  The name of the pet if known or given. Can be empty. & Both \\
\hline
DateTime &  The data and time of animal's arrival to shelter. YYYY-MM-DD HH:MM:SS format & Both\\
\hline
OutcomeType &  Why animal left shelter, e.g., Adoption, Died, Transfer, Return to Owner. & Training Only\\
\hline
OutcomeSubtype &  Further specification of outcome. For example, a euthanasia outcome could have suboutcome types of Agressive or Suffering. & Training Only\\
\hline
AnimalType & What kind of animal. Either Dog or Cat. & Both \\
\hline
SexuponOutcome &  The sex of the animal when outcome event occurred, e.g., Intact Female, Neutered Male & Both\\
\hline
AgeuponOutcome &  How old the animal was when outcome event occurred, e.g., 1 year, 3 weeks & Both\\
\hline
Breed &  The breed of the animal, e.g., Shetland Sheepdog Mix, Domestic Shorthair Mix & Both\\
\hline
Color &  The color of the animal, e.g., Brown Tabby, White, Yellow & Both\\
\hline
\end{tabular}
\end{center}

\clearpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Questions Addressed}
Below are a few questions that can be addressed through mining the SAO dataset. 

\begin{enumerate}
\item Which attribute correlates highest with each outcome?
\item Does time of year play a role in outcome? 
\item What is the ideal feature vector for an adoption outcome? 
\item Are named animals more likely to be returned to owner?
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Data Mining Approach}
Given the set of project questions above, the main task is to proceed from raw data to answers. This section provides a brief overview of the proposed approach and how the results will be interpreted. Before approaching a particular question, it may be necessary to preprocess the data. This proposal does not offer how this preprocessing will occur but rather notes the team is aware of the issue. 
\subsubsection*{Classification Algorithms}
The ultimate goal here will be to implement various classification algorithms for an overall performance comparison. 

\begin{itemize}
\item Bayes Classifier (primary)
\begin{itemize}
\item Implementation of Bayes classifier operates by computing conditional probabilities for each feature vector in the dataset and then using these calculations to determine the most likely outcome class for each test set feature vector. 
\end{itemize}
\item C4.5 Decision Tree (primary)
\begin{itemize}
\item Using the C4.5 decision tree primarily for rule generation. This could be implemented quite quickly on the SAO dataset once a 'names' file is created for the outcome classes and feature vectors. 
\end{itemize}
\item SVM (Secondary)
\item K-Nearest Neighbors (secondary)
\end{itemize}

\subsection*{Interpretation and Analysis}
The following methods will be used to judge the classification accuracy on the test data.
\begin{itemize}
\item ROC Curve (Precision Recall)
\item It appears the SAO project page allows submissions to test accuracy of submissions, so it should be possible to keep track of progress with multiple classification methods. 
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Team Members}
\begin{enumerate}
\item Hunter O'Rourke: orourke.hunter@gmail.com  -  254.368.0864
\item Ryan Taber: rtaber2@gmail.com  -  270.302.9381
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Related Work}

\paragraph*{Bayes:}
\subparagraph*{}Marc Boullé. Compression-Based Averaging of Selective Naive Bayes Classifiers. \textit{JMLR}, 8:1659–1685, July 2007a.
\subparagraph*{}Irina Rish. An empirical study of the naive bayes classifier. \textit{Proceedings of IJCAI-01 workshop on Empirical Methods in AI, International Joint Conference on Artifi-
cial Intelligence}, pages 41–46. American Association for Artificial Intelligence, 2001.

\paragraph*{SVM:}
\subparagraph*{}Yin-Wen Chang and Chih-Jen Lin. Feature Ranking Using Linear SVM. \textit{JMLR: Workshop and Conference Proceedings} 3:53-64, 2008.

\paragraph*{kNN:}
\subparagraph*{} Tsung-Hsien Chiang, Hung-Yi Lo, Shou-De Lin. A Ranking-based KNN Approach for Multi-Label Classification. \textit{JMLR: Workshop and Conference Proceedings} 25:81-96, 2012.

\subparagraph*{}Min-Ling Zhang and Zhi-Hua Zhou. Ml-knn: A lazy learning approach to multi-label learning. \textit{Pattern Recognition}, 40:2038–2048, July 2007.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage

\section{Time Table}
This section offers a simple timeline for subgoals in order to complete the overall project by April 28, 2016. 

\paragraph*{}


\begin{center}
\textbf{Table 3: Project Timeline\\}
\begin{tabular}{ |m{5cm} || c || c || c |}
\hline
\rowcolor{Gray}
 \textbf{Task} & \textbf{Assigned} & \textbf{Target Date} & \textbf{Status}\\
\hline\hline
 Find a partner & Ryan & 4.5.16 & Complete \\
\hline
 Set up project repo & Ryan & 4.5.16 & Complete \\
\hline
 Fine tune approach & Both & 4.8.16 & \\
\hline
 Preprocess datasets & Both & 4.10.16 & \\
\hline
 Begin report to complete background and approach sections & Both & 4.11.16 & \\
\hline
 Run Bayes Classifier & Ryan & 4.14.16 & \\
\hline
 Build decision tree and rules & Hunter & 4.14.16 & \\
\hline
 Run SVM & Ryan & 4.16.16 & \\
\hline
 Run KNN & Hunter & 4.16.16 & \\
\hline
 Analyze Results & Both & 4.21.16 & \\
\hline
 Finalize analysis charts and analysis section of report & Both & 4.26.16 & \\
\hline
 Final report submitted & Both & 4.28.16 & \\
\hline
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{References}
Other than the related works mentioned in section III, the main reference used for this proposal was the SAO project page. 
\begin{itemize}
\item\url {https://www.kaggle.com/c/shelter-animal-outcomes}
\end{itemize}

\raggedright{Additional references that will be used during implementation have come directly from CS 485 class materials and homework:} 
\begin{itemize}
\item\textbf{Data Mining and Analysis:} Fundamental Concepts and Algorithms, Zaki and Meira, 2014. 
\end{itemize}

\end{document}