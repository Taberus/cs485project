# README #

### Project Overview ###
This is the repo for CS 485: Data Mining class project. The selected problem is an open project taken from Kaggle. 

In particular, the project explores the Shelter Animal Outcomes dataset provided by the Austin Animal Center. 

### Team Members ###

* Ryan Taber, rtaber2@gmail.com 
* Hunter O'Rourke, orourke.hunter@gmail.com